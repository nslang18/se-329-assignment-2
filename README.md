The project organization is as follows:

Milestones represent the top level items in the WBS

Issues represent the second level items in the WBS and are assigned to their parent milestone

checklists are provided in the issues to represent all subtasks to each second level item in the WBS